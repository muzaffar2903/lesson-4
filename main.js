// Assignment 4

// Task1: Swap the case of each character of a string
// function charReverseHandler() {
//   // your code here
//   let str = prompt("Enter a character");
//   const UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//   const LOWER = "abcdefghijklmnopqrstuvwxyz";
//   const result = [];

//   for (let x = 0; x < str.length; x++) {
//     if (UPPER.includes(str[x])) {
//       result.push(str[x].toLowerCase());
//     } else if (LOWER.includes(str[x])) {
//       result.push(str[x].toUpperCase());
//     } else {
//       result.push(str[x]);
//     }
//   }
//   console.log(result.join(""));
// }

// charReverseHandler();

// Task2: Add items in a blank array and display the items
function createListHandler() {
  // your code here
  let x = 0;
  const array = Array();

  function add_element_to_array() {
    array[x] = document.getElementById("text1").value;
    alert(`Element: ${array[x]} Added at index ${x}`);
    x++;
    document.getElementById("text1").value = "";
  }
  add_element_to_array();

  function display_array() {
    let e = "<hr/>";

    for (let y = 0; y < array.length; y++) {
      e += `Element ${y} = ${array[y]}<br/>`;
    }
    document.getElementById("Result").innerHTML = e;
  }
  display_array();
}
createListHandler();

// Task3: Find duplicate values in a array
function findDublicatedHandler() {
  // your code here
  function find_duplicate_in_array(arra1) {
    const object = {};
    const result = [];

    arra1.forEach((item) => {
      if (!object[item]) object[item] = 0;
      object[item] += 1;
    });

    for (const prop in object) {
      if (object[prop] >= 2) {
        result.push(prop);
      }
    }

    return result;
  }

  console.log(
    find_duplicate_in_array([1, 2, -2, 4, 5, 4, 7, 8, 7, 7, 71, 3, 6])
  );
}

// Task4: Compute the union of two arrays
function unionHandler() {
  // your code here
  function union(arra1, arra2) {
    if (arra1 == null || arra2 == null) return void 0;

    const obj = {};

    for (let i = arra1.length - 1; i >= 0; --i) obj[arra1[i]] = arra1[i];

    for (let i = arra2.length - 1; i >= 0; --i) obj[arra2[i]] = arra2[i];

    const res = [];

    for (const n in obj) {
      if (obj.hasOwnProperty(n)) res.push(obj[n]);
    }

    return res;
  }
  console.log(union([1, 2, 3], [100, 2, 1, 10]));
}

// Task5: Remove null, 0, blank, false, undefined and NaN values from an array
function unionHandler() {
  // your code here
  function filter_array(test_array) {
    let index = -1;
    const arr_length = test_array ? test_array.length : 0;
    let resIndex = -1;
    const result = [];

    while (++index < arr_length) {
      const value = test_array[index];

      if (value) {
        result[++resIndex] = value;
      }
    }

    return result;
  }
  console.log(filter_array([NaN, 0, 15, false, -22, "", undefined, 47, null]));
}

// Task6: Returns a passed string with letters in alphabetical order
function reverseStringHandler() {
  // your code here
  function alphabet_order(str) {
    return str.split("").sort().join("");
  }
  console.log(alphabet_order("webmaster"));
}
